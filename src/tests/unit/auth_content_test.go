package unittests

import (
	"gofastocloud_backend/app/auth"
	"testing"
)

func TestJWTContentAuthManager_GetAuthContentToken(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		c    *auth.JWTContentAuthManager
		args args
	}{
		{name: "valid",
			c:    &auth.JWTContentAuthManager{},
			args: args{data: "127.0.0.1"},
		},
	}
	for _, tt := range tests {
		_, err := tt.c.GetAuthContentToken(tt.args.data)
		AssertNoError(t, err)
	}
}

func TestBaseContentAuthMenager_GetAuthContentToken(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		b    *auth.BaseContentAuthMenager
		args args
	}{
		{name: "valid",
			b:    &auth.BaseContentAuthMenager{},
			args: args{data: "127.0.0.1"},
		},
	}
	for _, tt := range tests {
		_, err := tt.b.GetAuthContentToken(tt.args.data)
		AssertNoError(t, err)
	}
}
