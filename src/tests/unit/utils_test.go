package unittests

import (
	"gofastocloud_backend/app/utils"
	"net/http"
	"testing"
)

func TestIsDoublicate(t *testing.T) {

	tests := []struct {
		name string
		args []string
		want bool
	}{
		{
			name: "valid case 1",
			args: []string{"62a4a662c358afba71d591a4", "62a4a662c358afba71d591a5", "62a4a662c358afba71d591a6"},
			want: false,
		},
		{
			name: "valid case 2",
			args: []string{},
			want: false,
		},
		{
			name: "invalid case 1",
			args: []string{"62a4a662c358afba71d591a4", "62a4a662c358afba71d591a5", "62a4a662c358afba71d591a4"},
			want: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			AssertEqual(t, utils.IsDoublicate(tt.args), tt.want)
		})
	}
}

func TestRemoveTokenFromQuery(t *testing.T) {
	req, _ := http.NewRequest("GET", "http://test.com:8080/1/62f5ea7a9ee/0/master.m3u8?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9&age=100", nil)
	req2, _ := http.NewRequest("GET", "http://test.com:8080/1/62f5ea7a9ee/0/master.m3u8?last=washington&age=100", nil)
	req3, _ := http.NewRequest("GET", "http://test.com:8080/1/62f5ea7a9ee/0/master.m3u8", nil)
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name  string
		args  args
		query map[string]string
	}{
		{name: "Case 1",
			args: args{r: req},
			query: map[string]string{
				"age": "100",
			}},
		{name: "Case 2",
			args: args{r: req2},
			query: map[string]string{
				"age": "100",
			}},
		{name: "Case 3",
			args: args{r: req3},
			query: map[string]string{
				"age": "",
			}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			utils.RemoveArgFromQuery(tt.args.r, "token")
			AssertEqual(t, tt.args.r.URL.Query().Get("token"), "")
			AssertEqual(t, tt.args.r.URL.Query().Get("age"), tt.query["age"])
		})
	}
}
