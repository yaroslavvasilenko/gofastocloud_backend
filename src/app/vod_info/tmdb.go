package vod_info

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	tmdb "github.com/cyruzin/golang-tmdb"
	"gitlab.com/fastogt/gofastogt"
)

type TmdbVodInfo struct {
	IVodInfo
	ApiKey string
}

func (t TmdbVodInfo) SearchVodInfo(query, logo, background string) ([]VodInfoResults, error) {
	client, err := t.getTmdbConnect()
	if err != nil {
		return nil, err
	}
	movies, err := client.GetSearchMovies(query, nil)
	if err != nil {
		return nil, err
	}
	var vodsInfo []VodInfoResults
	for _, res := range movies.Results {
		var vodInfo VodInfoResults
		release, err := time.Parse("2006-01-02", res.ReleaseDate)
		if err != nil {
			log.Error(fmt.Sprintf("Can't parse date: %v, exception: %v", res.ReleaseDate, err))
			release = time.Now()
		}
		vodInfo.PrimeDate = gofastogt.Time2UtcTimeMsec(release)
		if res.BackdropPath == "" {
			vodInfo.TvgLogo = logo
		} else {
			vodInfo.TvgLogo = tmdb.GetImageURL(res.PosterPath, tmdb.W185)
		}
		if res.BackdropPath == "" {
			vodInfo.TvgBackground = background
		} else {
			vodInfo.TvgBackground = tmdb.GetImageURL(res.BackdropPath, tmdb.W1280)
		}

		vodInfo.ID = strconv.Itoa(int(res.ID))
		vodInfo.Description = res.Overview
		vodInfo.Name = res.OriginalTitle
		vodInfo.UserScore = res.VoteAverage
		vodsInfo = append(vodsInfo, vodInfo)

	}

	return vodsInfo, nil
}

func (t TmdbVodInfo) GetVodInfo(id, logo, background string) (*VodInfo, error) {
	client, err := t.getTmdbConnect()
	if err != nil {
		return nil, err
	}
	idMovie, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	res, err := client.GetMovieDetails(idMovie, nil)
	if err != nil {
		return nil, err
	}
	var vodInfo VodInfo
	release, err := time.Parse("2006-01-02", res.ReleaseDate)
	if err != nil {
		log.Error(fmt.Sprintf("Can't parse date: %v, exception: %v", res.ReleaseDate, err))
		release = time.Now()
	}
	vodInfo.PrimeDate = gofastogt.Time2UtcTimeMsec(release)
	if res.BackdropPath == "" {
		vodInfo.TvgLogo = logo
	} else {
		vodInfo.TvgLogo = tmdb.GetImageURL(res.PosterPath, tmdb.W185)
	}
	if res.BackdropPath == "" {
		vodInfo.TvgBackground = background
	} else {
		vodInfo.TvgBackground = tmdb.GetImageURL(res.BackdropPath, tmdb.W1280)
	}
	for _, c := range res.ProductionCountries {
		vodInfo.Сountry = c.Name
		break
	}
	vodInfo.Duration = gofastogt.DurationMsec(res.Runtime * 60 * 1000)
	vodInfo.ID = strconv.Itoa(int(res.ID))
	vodInfo.Name = res.OriginalTitle
	vodInfo.Description = res.Overview
	vodInfo.UserScore = res.VoteAverage
	return &vodInfo, nil
}

func (t TmdbVodInfo) getTmdbConnect() (*tmdb.Client, error) {
	tmdbClient, err := tmdb.Init(t.ApiKey)
	if err != nil {
		return nil, err
	}
	customClient := http.Client{
		Timeout: time.Second * 5,
	}
	tmdbClient.SetClientConfig(customClient)
	return tmdbClient, nil
}
