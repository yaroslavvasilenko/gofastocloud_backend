package app

import (
	"encoding/json"
	"net/http"
	"text/template"

	"gitlab.com/fastogt/gofastogt"
)

func respondWithError(w http.ResponseWriter, statusCode int, err gofastogt.ErrorJson) {
	payload := gofastogt.NewErrorResponse(err)
	response, errj := json.Marshal(payload)
	if errj != nil {
		panic(errj)
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	w.Write(response)
}

func respondWithStructJSON(w http.ResponseWriter, statusCode int, payload *gofastogt.OkResponse) {
	response, err := json.Marshal(payload)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	w.Write(response)
}

func respondWithOk(w http.ResponseWriter) {
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(nil))
}

func respondWithTemplate(w http.ResponseWriter, tmpl *template.Template, data interface{}) {
	if data != nil {
		tmpl.Execute(w, data)
	}
}

func respondAuthDialog(w http.ResponseWriter) {
	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted Area"`)
	w.WriteHeader(http.StatusUnauthorized)
}
