package app

import (
	"encoding/json"
	"gofastocloud_backend/app/webrtc_in"
	"gofastocloud_backend/app/webrtc_out"

	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
)

func (app *App) OnClientStateChanged(cl *media.FastoCloudNodeClient, status gofastocloud_base.ConnectionStatus) {
	log.Debugf("OnClientStateChanged: %d", status)
	if app.autostartStreams && status == gofastocloud_base.ACTIVE {
		autostartStream := app.getHardawareStreams()
		for _, data := range autostartStream {
			_, err := app.client.StartStream(&media.StartStreamRequest{Config: data})
			if err != nil {
				log.Errorf("error autostartStream. error: %v", err.Error())
			}
		}
		app.autostartStreams = false
	}
}

func (app *App) OnStreamStatisticReceived(client *media.FastoCloudNodeClient, statistic media.StreamStatisticInfo) {
	log.Debugf("OnStreamStatisticReceived: %s", statistic)
	app.UpdateStreamStatistics(&statistic)
	app.wsUpdatesManager.BroadcastStreamStatistic(statistic)

	// alert
	app.sendStreamStatisticAlert(statistic)
}

func (app *App) OnStreamSourcesChanged(client *media.FastoCloudNodeClient, source media.ChangedSourcesInfo) {
	log.Debugf("OnStreamSourcesChanged: %s", source)
	app.wsUpdatesManager.BroadcastStreamChangedSources(source)
}

func (app *App) OnStreamMlNotification(client *media.FastoCloudNodeClient, notify media.MlNotificationInfo) {
	log.Debugf("OnStreamMlNotification: %s", notify)
	app.wsUpdatesManager.BroadcastMlNotification(notify)
}

func (app *App) OnServiceStatisticReceived(client *media.FastoCloudNodeClient, statistic media.ServiceStatisticInfo) {
	log.Debugf("OnServiceStatisticReceived: %s", statistic)
	app.wsUpdatesManager.BroadcastMediaServiceStatistic(statistic)

	// alert
	app.sendServiceStatisticAlert(statistic)
}

func (app *App) OnStreamResultReady(client *media.FastoCloudNodeClient, result media.ResultStreamInfo) {
	log.Debugf("OnStreamResultReady: %s", result)
	app.wsUpdatesManager.BroadcastResultStream(result)
}

func (app *App) OnWebRTCOutInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutInitInfo) {
	log.Debugf("OnWebRTCOutInitReceived: %s", web)
	pid := webrtc_out.PeerId(web.Init.ConnectionId)
	peer := app.wsOutWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCInit(web.Init)
	}
}

func (app *App) OnWebRTCOutDeInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutDeInitInfo) {
	log.Debugf("OnWebRTCOutDeinitReceived: %s", web)
	pid := webrtc_out.PeerId(web.DeInit.ConnectionId)
	peer := app.wsOutWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCDeInit(web.DeInit)
	}
}

func (app *App) OnWebRTCOutSdpReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutSdpInfo) {
	log.Debugf("OnWebRTCOutSdpReceived: %s", web)
	pid := webrtc_out.PeerId(web.Description.ConnectionId)
	peer := app.wsOutWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCSdp(web.Description)
	}
}

func (app *App) OnWebRTCOutIceReceived(client *media.FastoCloudNodeClient, web media.WebRTCOutIceInfo) {
	log.Debugf("OnWebRTCOutIceReceived: %s", web)
	pid := webrtc_out.PeerId(web.Ice.ConnectionId)
	peer := app.wsOutWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCIce(web.Ice)
	}
}

func (app *App) OnWebRTCInInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCInInitInfo) {
	log.Debugf("OnWebRTCInInitReceived: %s", web)
	pid := webrtc_in.PeerId(web.Init.ConnectionId)
	peer := app.wsInWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCInit(web.Init)
	}
}

func (app *App) OnWebRTCInDeInitReceived(client *media.FastoCloudNodeClient, web media.WebRTCInDeInitInfo) {
	log.Debugf("OnWebRTCInDeinitReceived: %s", web)
	pid := webrtc_in.PeerId(web.DeInit.ConnectionId)
	peer := app.wsInWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCDeInit(web.DeInit)
	}
}

func (app *App) OnWebRTCInSdpReceived(client *media.FastoCloudNodeClient, web media.WebRTCInSdpInfo) {
	log.Debugf("OnWebRTCInSdpReceived: %s", web)
	pid := webrtc_in.PeerId(web.Description.ConnectionId)
	peer := app.wsInWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCSdp(web.Description)
	}
}

func (app *App) OnWebRTCInIceReceived(client *media.FastoCloudNodeClient, web media.WebRTCInIceInfo) {
	log.Debugf("OnWebRTCInIceReceived: %s", web)
	pid := webrtc_in.PeerId(web.Ice.ConnectionId)
	peer := app.wsInWebRTCManager.GetWsPeerByID(web.Id, pid)
	if peer != nil {
		peer.SendWebRTCIce(web.Ice)
	}
}

func (app *App) OnQuitStatusStream(client *media.FastoCloudNodeClient, status media.QuitStatusInfo) {
	log.Debugf("OnQuitStatusStream: %s", status)
	peer := app.wsOutWebRTCManager.GetSrtreamPeersByID(status.Id)
	for item := range peer {
		app.wsOutWebRTCManager.UnRegisterWsWebRTCClientAuthConnection(item)
	}
	app.wsUpdatesManager.BroadcastStreamQuitStatus(status)
	app.RemoveActiveStream(status.Id)

	// alert
	app.sendStopStreamAlert(status)
}

func (app *App) OnPingReceived(client *media.FastoCloudNodeClient, ping media.PingInfo) {
	log.Debugf("OnPingReceived: %s", ping)
}

func (app *App) sendStreamStatisticAlert(statistic media.StreamStatisticInfo) {

}

func (app *App) sendServiceStatisticAlert(statistic media.ServiceStatisticInfo) {

}

func (app *App) sendStopStreamAlert(status media.QuitStatusInfo) {
	sid := string(status.Id)
	raw, err := app.storage.GetStream(sid)
	if err != nil {
		return
	}

	iStream := gofastocloud_models.IStream{}
	if err = json.Unmarshal(raw, &iStream); err != nil {
		return
	}
	var adresses []string
	if iStream.TypeStream == media.STREAM_TYPE_PROXY ||
		iStream.TypeStream == media.STREAM_TYPE_VOD_PROXY {
		return
	}
	adresses = handleGetNotificationAdresses(raw)
	if adresses == nil {
		return
	}
	go app.SendStopStreamNotification(sid, iStream.Name, app.config.Settings.Host, status.ExitStatus, adresses)
}

func handleGetNotificationAdresses(raw json.RawMessage) []string {
	var adresses []string
	var hwStream gofastocloud_models.HardwareStream
	if err := json.Unmarshal(raw, &hwStream); err != nil {
		return nil
	}
	for _, n := range hwStream.NotificationStreamContact {
		if n.NotificationStreamType == front.STREAM_FINISHED {
			adresses = append(adresses, n.Email)
		}
	}
	return adresses
}
