package app

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"text/template"

	"gofastocloud_backend/app/common"
	"gofastocloud_backend/app/errorgt"
	"gofastocloud_backend/app/updates"

	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
	"gitlab.com/fastogt/gofastocloud_http/fastocloud/public"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastogt"
)

// GET methods

func (app *App) Index(w http.ResponseWriter, r *http.Request) {
	version := updates.GetVersionShort()
	scheme := app.config.Settings.PreferredUrlScheme

	link := fmt.Sprintf("%s://v%s.%s.fastocloud.com", scheme, version, scheme)
	tmpl, err := template.New("index.html").ParseFiles(GetTemplatePath("index.html"))
	if err != nil {
		msg := "error tempalte load"
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	type response struct {
		Link string
	}
	respondWithTemplate(w, tmpl, response{Link: link})
}

func (app *App) Version(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	vers := public.VersionStruct{HumanReadable: updates.GetVersion(), Short: updates.GetVersionShort()}
	resp := public.Version{Version: vers}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
}

func (app *App) Playlist(w http.ResponseWriter, r *http.Request) {
	/*err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}*/
	
	playlist := app.makePlaylist()
	if playlist != nil {
		w.Header().Set("Content-Type", "application/x-mpequrl")
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, string(playlist))
		return
	}

	msg := "playlist is empty"
	respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ProtectedPlaylist(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		respondAuthDialog(w)
		return
	}

	playlist := app.makePlaylist()
	if playlist != nil {
		w.Header().Set("Content-Type", "application/x-mpequrl")
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, string(playlist))
		return
	}
	w.WriteHeader(http.StatusNotFound)
}

func (app *App) makePlaylist() []byte {
	dbStreams, err := app.storage.GetStreams()
	if err != nil {
		return nil
	}
	playlist := []byte("#EXTM3U\n")
	for _, dbStream := range dbStreams {
		var istream gofastocloud_models.IStream
		if err := json.Unmarshal(dbStream, &istream); err != nil {
			continue
		}
		var group, outputUri string
		for _, g := range istream.Groups {
			group = g
			break
		}
		for _, uri := range istream.Output {
			outputUri = uri.OutputUrl.Uri
			break
		}
		str := fmt.Sprintf("#EXTINF:-1 tvg-id=\"%s\" tvg-name=\"%s\" tvg-logo=\"%s\" group-title=\"%s\",%s\n%s\n",
			istream.TVGID, istream.TVGName, istream.TVGLogo, group, istream.Name, outputUri)
		playlist = append(playlist, []byte(str)...)
	}
	return playlist
}

func (app *App) Info(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	type InfoResponce struct {
		HlsHost  string `json:"hls_host"`
		VodsHost string `json:"vods_host"`
		CodsHost string `json:"cods_host"`
		Scheme   string `json:"scheme"`
	}

	hls := app.config.Settings.HlsHost.GetTrueHost()
	if hls == nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	vods := app.config.Settings.VodsHost.GetTrueHost()
	if vods == nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	cods := app.config.Settings.CodsHost.GetTrueHost()
	if cods == nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := InfoResponce{HlsHost: hls.String(), VodsHost: vods.String(), CodsHost: cods.String(), Scheme: "http"}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
}

func (app *App) Stats(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	runtime := app.client.GetRuntimeStats()
	version := app.client.GetVersion()
	project := app.client.GetProject()
	exp := app.client.GetExpirationTime()
	os := app.client.GetOS()
	resp := public.NewFullStatService(*runtime, *app.client.GetStatus(), *version, *project, *exp, *os)
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(*resp))
}

func (app *App) ServerStats(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}
	app.getServerStats()
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(app.serverStats))
}

func (app *App) ServiceLogs(w http.ResponseWriter, r *http.Request) {
	/*err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}*/

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	selfUrl := app.makeSelfUrl(kServiceLogsUploadRoute, r.Host)
	logs := media.NewGetLogServiceRequest(selfUrl)
	respWait := make(chan gofastocloud_base.Response)
	_, err := app.client.GetLogServiceWithCallback(logs, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		logPath, err := app.getServerLogsFilePath()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		http.ServeFile(w, r, *logPath)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ServerLogs(w http.ResponseWriter, r *http.Request) {
	/*err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}*/

	stabledLogPath, err := gofastogt.StableFilePath(app.config.Settings.LogPath)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	http.ServeFile(w, r, *stabledLogPath)
}

func (app *App) ExternalServiceUploadLogs(w http.ResponseWriter, r *http.Request) {
	file_path, err := app.getServerLogsFilePath()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Create a new file in the uploads directory
	dst, err := os.Create(*file_path)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	// Copy the uploaded file to the filesystem
	// at the specified destination
	dst.Write([]byte(`<pre>`))
	_, err = io.Copy(dst, r.Body)
	dst.Write([]byte(`</pre>`))
	dst.Close()
	if err != nil {
		os.Remove(*file_path)
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(nil))
}

// POST methods

func (app *App) ProbeIn(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.ProbeRequestIn
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	probe := media.NewProbeInStreamRequest(body.Url)
	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.ProbeInStreamWithCallback(probe, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		var ffrpobe media.ProbeData
		if err := json.Unmarshal(*resp.Result, &ffrpobe); err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonParseResponse(&msg))
			return
		}

		resp := public.Probe{Probe: ffrpobe, Info: *ffrpobe.MediaUrlInfo()}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ProbeOut(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.ProbeRequestOut
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	probe := media.NewProbeOutStreamRequest(body.Url)
	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.ProbeOutStreamWithCallback(probe, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		/*if err == media.ErrNotActive {
			respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
			return
		}*/
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		var ffrpobe media.ProbeData
		if err := json.Unmarshal(*resp.Result, &ffrpobe); err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonParseResponse(&msg))
			return
		}

		resp := public.Probe{Probe: ffrpobe, Info: *ffrpobe.MediaUrlInfo()}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) MountS3Bucket(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.MountBucketRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	mount := media.NewMountS3BucketRequest(body.Name, body.Path, body.Key, body.Secret)
	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.MountS3BucketWithCallback(mount, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) UnMountS3Bucket(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.UnmountBucketRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	mount := media.NewUnMountS3BucketRequest(body.Path)
	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.UnMountS3BucketWithCallback(mount, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		respondWithOk(w)
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) S3BucketList(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	if !app.client.IsActive() {
		respondWithError(w, http.StatusServiceUnavailable, errorgt.MakeErrorJsonServiceNotActived(nil))
		return
	}

	respWait := make(chan gofastocloud_base.Response)
	scan := media.NewScanS3BucketsRequest()
	_, err = app.client.ScanS3BucketsWithCallback(scan, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		var result public.GetBucket
		if err := json.Unmarshal(*resp.Result, &result); err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonParseResponse(&msg))
			return
		}
		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(resp))
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) ScanFolder(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var body public.ScanFolderRequest
	err = json.Unmarshal(sBody, &body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	scan := media.NewScanFolderRequest(body.Directory, body.Extensions)
	respWait := make(chan gofastocloud_base.Response)
	_, err = app.client.ScanFolderWithCallback(scan, func(req *gofastocloud_base.Request, resp *gofastocloud_base.Response) {
		respWait <- *resp
	})

	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	resp := <-respWait
	if resp.IsMessage() {
		var result public.ScanFolder
		if err := json.Unmarshal(*resp.Result, &result); err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonParseResponse(&msg))
			return
		}

		respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(result))
		return
	}
	msg := resp.Error.Error()
	respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
}

func (app *App) Sync(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	var result public.SyncConfigRequest
	if err := json.Unmarshal(sBody, &result); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.active_streams_mutex.Lock()
	streams := make(map[media.StreamId]*StreamInfo)
	for _, entry := range result.Configs {
		var config media.BaseConfig
		err := json.Unmarshal(entry, &config)
		if err == nil {
			val, ok := app.active_streams[config.Id]
			streamConfig := common.NewStreamConfig(entry, config)
			if !ok {
				streams[config.Id] = NewStreamInfo(streamConfig, nil)
			} else {
				if !val.IsActive() {
					streams[config.Id] = NewStreamInfo(streamConfig, nil)
				} else {
					if val.HasConfig() {
						streams[config.Id] = val
					} else {
						streams[config.Id] = NewStreamInfo(streamConfig, val.Statistic)
					}
				}
			}
		} else {
			log.Error("parse stream error: %s", err.Error())
		}
	}
	app.active_streams = streams
	app.active_streams_mutex.Unlock()

	respondWithOk(w)
}
