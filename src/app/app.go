package app

import (
	"context"
	"fmt"
	"gofastocloud_backend/app/auth"
	"gofastocloud_backend/app/common"
	"gofastocloud_backend/app/storage"
	"gofastocloud_backend/app/updates"
	"gofastocloud_backend/app/utils"
	"gofastocloud_backend/app/vod_info"
	"gofastocloud_backend/app/webrtc_in"
	"gofastocloud_backend/app/webrtc_out"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastogt"
)

const kRuntimeFolder = "runtime_folder"
const kStreamsFolder = kRuntimeFolder + "/streams"
const kUploadsFolder = kRuntimeFolder + "/uploads"
const kDBFolder = kRuntimeFolder + "/db"
const kStreamsDB = "/gofastocloud.db"
const kSeasonsDB = "/seasons_gofastocloud.db"
const kSerialsDB = "/serials_gofastocloud.db"

const kMediaPrefix = "/media"
const kServerPrefix = "/server"
const kServiceLogsUploadRoute = kMediaPrefix + "/logs/upload"

const kStreamConfigUploadRoute = kMediaPrefix + "/stream/config/upload"
const kStreamLogsUploadRoute = kMediaPrefix + "/stream/logs/upload"
const kStreamPipelineUploadRoute = kMediaPrefix + "/stream/pipeline/upload"

const kMaxLogfileSize = 10 * 1024 * 1024

type IWatcher interface {
	OnConfigUpdated()
	StopServer()
}

type App struct {
	gofastogt.IEventLoopObserver
	media.IFastoCloudNodeClientHandler
	common.IStreamWatcher

	http      *http.Server
	hls_http  *http.Server
	vods_http *http.Server
	cods_http *http.Server
	client    *WrapNode

	auth        auth.IAuthManager
	masterAuth  auth.MasterAuth
	contentAuth auth.ContentAuthManager

	wsOutWebRTCManager *webrtc_out.WebRTCManagerWs
	wsInWebRTCManager  *webrtc_in.WebRTCManagerWs
	wsUpdatesManager   *updates.UpdatesManagerWs

	config  Config
	logFile *os.File

	loop       *gofastogt.EventLoop
	stats_time *gofastogt.Interval

	active_streams_mutex sync.Mutex
	active_streams       map[media.StreamId]*StreamInfo

	runtime_fodler *string
	streams_fodler *string
	uploads_folder *string

	configPath string

	watcher          IWatcher
	storage          storage.IStorage
	autostartStreams bool

	blackList map[string]bool

	serverStatsMutex sync.Mutex
	serverStats      updates.ServerStats

	vodInfo vod_info.IVodInfo
}

func NewApp(configPath string, watcher IWatcher) *App {
	return &App{configPath: configPath, watcher: watcher}
}

func (app *App) OnStreamStarted(config *common.StreamConfig) {
	app.active_streams_mutex.Lock()
	cfg := config.GetConfig()
	if val, ok := app.active_streams[cfg.Id]; ok {
		app.active_streams[cfg.Id] = NewStreamInfo(config, val.Statistic)
	} else {
		app.active_streams[cfg.Id] = NewStreamInfo(config, nil)
	}
	app.active_streams_mutex.Unlock()
}

func (app *App) FindActiveStream(sid media.StreamId) *StreamInfo {
	app.active_streams_mutex.Lock()
	defer app.active_streams_mutex.Unlock()
	found, ok := app.active_streams[sid]
	if !ok {
		return nil
	}
	return found
}

func (app *App) UpdateStreamStatistics(stat *media.StreamStatisticInfo) {
	app.active_streams_mutex.Lock()
	if val, ok := app.active_streams[stat.Id]; ok {
		app.active_streams[stat.Id] = NewStreamInfo(val.Config, stat)
	} else {
		app.active_streams[stat.Id] = NewStreamInfo(nil, stat)
	}
	app.active_streams_mutex.Unlock()
}

func (app *App) RemoveActiveStream(sid media.StreamId) {
	app.active_streams_mutex.Lock()
	delete(app.active_streams, sid)
	app.active_streams_mutex.Unlock()
}

func (app *App) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		// extra handling here
		cancel()
	}()

	app.client.Stop()

	if err := app.http.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
}

func (app *App) GetNodeHost() *gofastogt.HostAndPort {
	return app.config.Settings.Node.GetHost()
}

func (app *App) Initialize(config Config) {
	masterPass := strings.Split(config.Settings.Master, ":")
	if len(masterPass) == 2 {
		master := auth.Master{masterPass[0]: masterPass[1]}
		app.masterAuth = *auth.NewMasterAuth(master)
	} else {
		log.Fatal("Master field required")
	}

	app.contentAuth = auth.MakeAuthContent(config.Settings.AuthContent)
	stabledLogPath, err := gofastogt.StableFilePath(config.Settings.LogPath)
	if err != nil {
		log.Error(err)
	}

	app.logFile, err = gofastogt.InitLogFile(*stabledLogPath, kMaxLogfileSize)
	if err != nil {
		log.Error(err)
	} else {
		log.SetOutput(app.logFile)
	}

	level, err := log.ParseLevel(config.Settings.LogLevel)
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)
	log.Infof("Running %s", updates.GetVersion())

	app.config = config
	if config.Settings.Auth != nil {
		userPassword := strings.Split(*config.Settings.Auth, ":")
		if len(userPassword) == 2 {
			users := auth.Users{userPassword[0]: userPassword[1]}
			app.auth = auth.NewBasicAuthManager(users)
		}
	}

	if app.auth == nil {
		app.auth = auth.NewNoAuthManager()
	}

	app.serverStatsMutex = sync.Mutex{}
	app.active_streams_mutex = sync.Mutex{}
	app.active_streams = make(map[media.StreamId]*StreamInfo)

	client := NewWrapNode(app.config.Settings.Node, app)
	app.loop = gofastogt.NewEventLoop(app)

	var webrtco *webrtc_out.WebRTCSettings
	var webrtci *webrtc_in.WebRTCSettings
	if app.config.Settings.WebRTC != nil {
		webrtco = &webrtc_out.WebRTCSettings{Stun: app.config.Settings.WebRTC.Stun, Turn: app.config.Settings.WebRTC.Turn}
		webrtci = &webrtc_in.WebRTCSettings{Stun: app.config.Settings.WebRTC.Stun, Turn: app.config.Settings.WebRTC.Turn}
	}
	app.wsOutWebRTCManager = webrtc_out.NewWebRTCManagerWs(client, webrtco)
	app.wsInWebRTCManager = webrtc_in.NewWebRTCManagerWs(client, app, webrtci)
	app.wsUpdatesManager = updates.NewUpdatesManagerWs()
	app.client = client

	runtime_fodler, streams_fodler, uploads_folder, db_folder, err := app.initFolders()
	if err != nil {
		log.Errorf("init filders error: %s", err.Error())
	}
	app.runtime_fodler = runtime_fodler
	app.streams_fodler = streams_fodler
	app.uploads_folder = uploads_folder
	// routes
	app.http = app.initializeRoutes()

	hlsHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		host, _, err := gofastogt.GetIPFromRequest(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if app.IsInBlackHost(*host) {
			w.WriteHeader(http.StatusForbidden)
			return
		}
		app.ServeHlsRequest(w, r)
	})
	app.hls_http = utils.NewHttpServer(config.Settings.HlsHost.Host, config.Settings.Cors, hlsHandler)
	vodsHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		host, _, err := gofastogt.GetIPFromRequest(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if app.IsInBlackHost(*host) {
			w.WriteHeader(http.StatusForbidden)
			return
		}
		app.ServeVodsRequest(w, r)
	})
	app.vods_http = utils.NewHttpServer(config.Settings.VodsHost.Host, config.Settings.Cors, vodsHandler)
	codsHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		host, _, err := gofastogt.GetIPFromRequest(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if app.IsInBlackHost(*host) {
			w.WriteHeader(http.StatusForbidden)
			return
		}
		app.ServeCodsRequest(w, r)
	})
	app.cods_http = utils.NewHttpServer(config.Settings.CodsHost.Host, config.Settings.Cors, codsHandler)

	app.blackList = make(map[string]bool)
	for _, element := range config.Settings.BlackList {
		app.blackList[element.IP] = true
	}

	store, err := storage.NewPudge(*db_folder+kStreamsDB, *db_folder+kSeasonsDB, *db_folder+kSerialsDB)
	if err != nil {
		log.Fatalf("init store error: %s", err.Error())
	}
	app.storage = store
	app.autostartStreams = true

	app.serverStats = updates.MakeDefStats()
	app.vodInfo = makeVodInfoByType(config.Settings.VodInfo)
}

func (app *App) IsInBlackHost(host string) bool {
	if _, ok := app.blackList[host]; ok {
		return true
	}

	return false
}

func (app *App) initializeRoutes() *http.Server {
	router := mux.NewRouter()
	router.HandleFunc("/", app.Index).Methods("GET")
	router.PathPrefix("/install/").Handler(http.StripPrefix("/install/", http.FileServer(http.Dir(updates.ShareAbsFolder+"/install"))))
	router.PathPrefix("/uploads/").Handler(http.StripPrefix("/uploads/", http.FileServer(http.Dir(*app.uploads_folder))))

	router.HandleFunc(kMediaPrefix+"/stream/start", app.StartStream).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/start_cached", app.StartCached).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/restart", app.RestartStream).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/stop", app.StopStream).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/clean", app.CleanStream).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/change_source", app.ChangeSourceStream).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/inject_master_source", app.InjectMasterSourceStream).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/remove_master_source", app.RemoveMasterSourceStream).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/stats/{id}", app.StreamStats).Methods("GET")

	router.HandleFunc(kMediaPrefix+"/stream/config/{id}", app.StreamConfig).Methods("GET")
	router.HandleFunc(kStreamConfigUploadRoute+"/{id}", app.ExternalStreamUploadConfig).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/logs/{id}", app.StreamLogs).Methods("GET")
	router.HandleFunc(kStreamLogsUploadRoute+"/{id}", app.ExternalStreamUploadLogs).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/stream/pipeline/{id}", app.StreamPipeline).Methods("GET")
	router.HandleFunc(kStreamPipelineUploadRoute+"/{id}", app.ExternalStreamUploadPipeline).Methods("POST")

	// db
	router.HandleFunc(kServerPrefix+"/db/stream/upload_file", app.DBPlaylistAdd).Methods("POST")
	router.HandleFunc(kServerPrefix+"/db/stream/add", app.DBStreamAdd).Methods("POST")
	router.HandleFunc(kServerPrefix+"/db/stream/edit", app.DBStreamUpdate).Methods("PATCH")
	router.HandleFunc(kServerPrefix+"/db/stream/remove/{id}", app.DBStreamRemove).Methods("DELETE")
	router.HandleFunc(kServerPrefix+"/db/stream/list", app.DBStreamsList).Methods("GET")

	router.HandleFunc(kServerPrefix+"/db/stream/live/list", app.DBGetLiveList).Methods("GET")
	router.HandleFunc(kServerPrefix+"/db/stream/live/{id}", app.DBGetLiveStream).Methods("GET")

	router.HandleFunc(kServerPrefix+"/db/stream/vod/list", app.DBGetVodsList).Methods("GET")
	router.HandleFunc(kServerPrefix+"/db/stream/vod/{id}", app.DBGetVodStream).Methods("GET")

	router.HandleFunc(kServerPrefix+"/db/stream/episode/list", app.DBGetEpisodesList).Methods("GET")
	router.HandleFunc(kServerPrefix+"/db/stream/episode/{id}", app.DBGetEpisodeStream).Methods("GET")

	router.HandleFunc(kServerPrefix+"/db/stream/view/{id}", app.DBStreamsAddView).Methods("POST")

	router.HandleFunc(kServerPrefix+"/db/season/add", app.DBSeasonAdd).Methods("POST")
	router.HandleFunc(kServerPrefix+"/db/season/edit", app.DBSeasonUpdate).Methods("PATCH")
	router.HandleFunc(kServerPrefix+"/db/season/remove/{id}", app.DBSeasonRemove).Methods("DELETE")
	router.HandleFunc(kServerPrefix+"/db/season/list", app.DBSeasonsList).Methods("GET")
	router.HandleFunc(kServerPrefix+"/db/season/{id}", app.DBGetSeason).Methods("GET")

	router.HandleFunc(kServerPrefix+"/db/serial/add", app.DBSerialAdd).Methods("POST")
	router.HandleFunc(kServerPrefix+"/db/serial/edit", app.DBSerialUpdate).Methods("PATCH")
	router.HandleFunc(kServerPrefix+"/db/serial/remove/{id}", app.DBSerialRemove).Methods("DELETE")
	router.HandleFunc(kServerPrefix+"/db/serial/list", app.DBSerialsList).Methods("GET")
	router.HandleFunc(kServerPrefix+"/db/serial/{id}", app.DBGetSerial).Methods("GET")

	router.HandleFunc(kServerPrefix+"/db/content", app.DBContent).Methods("GET")

	// db actions
	router.HandleFunc(kMediaPrefix+"/db/stream/start/{id}", app.DBStreamStart).Methods("GET")

	// media service
	router.HandleFunc(kMediaPrefix+"/streams_stats", app.StreamsStats).Methods("GET")
	router.HandleFunc(kMediaPrefix+"/streams_configs", app.StreamsConfigs).Methods("GET") // active owned

	router.HandleFunc(kMediaPrefix+"/stats", app.Stats).Methods("GET")
	router.HandleFunc(kMediaPrefix+"/logs", app.ServiceLogs).Methods("GET")
	router.HandleFunc(kServiceLogsUploadRoute, app.ExternalServiceUploadLogs).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/probe_in", app.ProbeIn).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/probe_out", app.ProbeOut).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/sync", app.Sync).Methods("POST")
	// others
	router.HandleFunc(kMediaPrefix+"/folder/scan", app.ScanFolder).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/s3bucket/mount", app.MountS3Bucket).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/s3bucket/unmount", app.UnMountS3Bucket).Methods("POST")
	router.HandleFunc(kMediaPrefix+"/s3bucket/list", app.S3BucketList).Methods("GET")

	// websockets
	router.HandleFunc("/webrtc_out", func(w http.ResponseWriter, r *http.Request) {
		app.ServeWebRTCWebRTCOut(w, r)
	})
	router.HandleFunc("/webrtc_in", func(w http.ResponseWriter, r *http.Request) {
		app.ServeWebRTCWebRTCIn(w, r)
	})
	router.HandleFunc("/updates", func(w http.ResponseWriter, r *http.Request) {
		app.ServeUpdatesWs(w, r)
	})

	//server routes
	//master auth
	router.HandleFunc(kServerPrefix+"/config", app.GetConfig).Methods("GET")
	router.HandleFunc(kServerPrefix+"/config", app.PostConfig).Methods("POST")
	router.HandleFunc(kServerPrefix+"/stop", app.ServerStop).Methods("GET")
	//auth
	router.HandleFunc(kServerPrefix+"/stats", app.ServerStats).Methods("GET")
	router.HandleFunc(kServerPrefix+"/logs", app.ServerLogs).Methods("GET")
	router.HandleFunc(kServerPrefix+"/info", app.Info).Methods("GET")
	router.HandleFunc(kServerPrefix+"/version", app.Version).Methods("GET")
	router.HandleFunc(kServerPrefix+"/stream/embed", app.StreamEmbed).Methods("GET")
	router.HandleFunc(kServerPrefix+"/stream/player", app.StreamPlayer).Methods("GET")
	router.HandleFunc(kServerPrefix+"/stream/token", app.GetContentToken).Methods("GET")      // DOCS
	router.HandleFunc(kServerPrefix+"/stream/token", app.GetContentTokenByIP).Methods("POST") // DOCS
	router.HandleFunc(kServerPrefix+"/playlist.m3u", app.Playlist).Methods("GET")
	router.HandleFunc(kServerPrefix+"/protected_playlist.m3u", app.ProtectedPlaylist).Methods("GET")
	router.HandleFunc(kServerPrefix+"/video/upload", app.UploadVideoFile).Methods("POST")
	router.HandleFunc(kServerPrefix+"/video/remove", app.RemoveVideoFile).Methods("POST")
	router.HandleFunc(kServerPrefix+"/tmdb/search", app.GetTmdbContent).Methods("POST")
	router.HandleFunc(kServerPrefix+"/tmdb/search/{id}", app.GetTmdbContentByID).Methods("POST")

	httpHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		host, _, err := gofastogt.GetIPFromRequest(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if app.IsInBlackHost(*host) {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		router.ServeHTTP(w, r)
	})
	server := utils.NewHttpServer(app.config.Settings.Host, app.config.Settings.Cors, httpHandler)
	return server
}

func (app *App) initFolders() (*string, *string, *string, *string, error) {
	home, err := gofastogt.StableDirPath("~/.gofastocloud")
	if err != nil {
		return nil, nil, nil, nil, err
	}

	runtime_folder := filepath.Join(*home, kRuntimeFolder)
	err = os.MkdirAll(runtime_folder, os.ModePerm)
	if err != nil {
		return nil, nil, nil, nil, err
	}

	streams_fodler := filepath.Join(*home, kStreamsFolder)
	err = os.MkdirAll(streams_fodler, os.ModePerm)
	if err != nil {
		return nil, nil, nil, nil, err
	}

	uploads_folder := filepath.Join(*home, kUploadsFolder)
	err = os.Mkdir(uploads_folder, os.ModePerm)
	if err != nil {
		if !os.IsExist(err) {
			return nil, nil, nil, nil, err
		}
	}

	db_folder := filepath.Join(*home, kDBFolder)
	err = os.Mkdir(db_folder, os.ModePerm)
	if err != nil {
		if !os.IsExist(err) {
			return nil, nil, nil, nil, err
		}
	}

	return &runtime_folder, &streams_fodler, &uploads_folder, &db_folder, nil
}

func (app *App) DeInitialize() {
	app.hls_http.Close()
	app.cods_http.Close()
	app.vods_http.Close()
	app.http.Close()

	if app.storage != nil {
		app.storage.Close()
		app.storage = nil
	}

	log.Infof("Quiting ...")
	if app.logFile != nil {
		app.logFile.Close()
		app.logFile = nil
	}
}

func (app *App) Run() {
	go app.loop.Run()
	go app.client.Run()

	hlsLoop := func() {
		log.Info("Started hls loop")
		if err := app.hls_http.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Errorf("hls loop error: %s", err.Error())
		}
		log.Info("Finished hls loop")
	}
	go hlsLoop()

	codsLoop := func() {
		log.Info("Started cods loop")
		if err := app.cods_http.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Errorf("cods loop error: %s", err.Error())
		}
		log.Info("Finished cods loop")
	}
	go codsLoop()

	vodsLoop := func() {
		log.Info("Started vods loop")
		if err := app.vods_http.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Errorf("vods loop error: %s", err.Error())
		}
		log.Info("Finished vods loop")
	}
	go vodsLoop()

	log.Info("Started http loop")
	if err := app.http.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Errorf("http loop error: %s", err.Error())
	}
	log.Info("Finished http loop")

	app.loop.Stop()
}

func (app *App) PreLooped(loop *gofastogt.EventLoop) {
	stater := func() {
		app.getServerStats()
		app.wsUpdatesManager.BroadcastServiceStatistic(app.serverStats)
	}
	app.stats_time = loop.AddInterval(stater, time.Duration(10)*time.Second)
}

func (app *App) PostLooped(loop *gofastogt.EventLoop) {
	app.loop.ClearInterval(app.stats_time)
}

func (app *App) makeSelfUrl(url string, host string) string {
	return fmt.Sprintf("%s://%s%s", app.config.Settings.PreferredUrlScheme, host, url)
}

func (app *App) getServerStats() {
	app.serverStatsMutex.Lock()
	app.serverStats = updates.GetStats(app.serverStats)
	app.serverStatsMutex.Unlock()
}
