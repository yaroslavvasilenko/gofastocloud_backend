package webrtc_in

import (
	"encoding/json"
	"fmt"

	"github.com/gorilla/websocket"
	"gitlab.com/fastogt/gofastocloud/media"
)

const kFrom = "from"
const kTo = "to"
const kSessionId = "session_id"
const kTracks = "tracks"
const kDescription = "description"
const kCandidate = "candidate"

type MessageCommand string

const (
	kNewCommand         MessageCommand = "new"
	kOfferCommand       MessageCommand = "offer"
	kAnswerCommand      MessageCommand = "answer"
	kCandidateCommand   MessageCommand = "candidate"
	kSessionInfoCommand MessageCommand = "session_info"
	kEnterCommand       MessageCommand = "enter"
	kLeaveCommand       MessageCommand = "leave"
	kByeCommand         MessageCommand = "bye"
)

type PeerId string

type PeerInfo struct {
	Id        PeerId
	Sid       media.StreamId
	UserAgent string
}

func NewPeerInfo(pid PeerId, user_agent string, sid media.StreamId) *PeerInfo {
	return &PeerInfo{Id: pid, Sid: sid, UserAgent: user_agent}
}

type IWsWebRTCClientConnectionClient interface {
	OnWsWebRTCClientConnectionClose(ws *WsWebRTCClientAuthConnection)
}

type WsWebRTCClientAuthConnection struct {
	conn   *websocket.Conn
	peer   *PeerInfo
	client IWsWebRTCClientConnectionClient
}

func (ws *WsWebRTCClientAuthConnection) GetSessionID() *string {
	if ws.peer == nil {
		return nil
	}
	return (*string)(&ws.peer.Sid)
}

func (ws *WsWebRTCClientAuthConnection) Close() error {
	if ws.conn == nil {
		return nil
	}
	if ws.client != nil {
		ws.client.OnWsWebRTCClientConnectionClose(ws)
	}
	return ws.conn.Close()
}

func (ws *WsWebRTCClientAuthConnection) WriteMessage(message string) error {
	return ws.conn.WriteJSON(map[string]interface{}{"message": message})
}

func NewWsWebRTCClientAuthConnection(connection *websocket.Conn, client IWsWebRTCClientConnectionClient) *WsWebRTCClientAuthConnection {
	result := WsWebRTCClientAuthConnection{conn: connection, peer: nil, client: client}
	return &result
}

func (ws *WsWebRTCClientAuthConnection) SendWebRTCInit(web media.WebRTCSubInitInfo) error {
	PeerId := PeerId(web.ConnectionId)
	streamID := ws.peer.Sid
	sessionID := ws.GetSessionID()
	raw, err := json.Marshal(map[string]interface{}{kFrom: streamID, kTo: PeerId, kSessionId: sessionID})
	if err != nil {
		return err
	}
	wsm := WSMessage{Type: kNewCommand, Data: raw}
	return ws.conn.WriteJSON(wsm)
}

func (ws *WsWebRTCClientAuthConnection) SendWebRTCSdp(web media.WebRTCSubSdpInfo) error {
	PeerId := PeerId(web.ConnectionId)
	streamID := ws.peer.Sid
	sessionId := ws.GetSessionID()
	raw, err := json.Marshal(map[string]interface{}{kFrom: streamID, kDescription: web, kTo: PeerId, kSessionId: sessionId})
	if err != nil {
		return err
	}

	sdpType, err := media.ParseWebRTCSdpType(web.Type)
	if err != nil {
		return err
	}

	if sdpType == media.OFFER {
		wsm := WSMessage{Type: kOfferCommand, Data: raw}
		return ws.conn.WriteJSON(wsm)
	} else if sdpType == media.ANSWER {
		wsm := WSMessage{Type: kAnswerCommand, Data: raw}
		return ws.conn.WriteJSON(wsm)
	}
	return fmt.Errorf("not handled sdp type: %s", web.Type)
}

func (ws *WsWebRTCClientAuthConnection) SendWebRTCIce(web media.WebRTCSubIceInfo) error {
	PeerId := PeerId(web.ConnectionId)
	streamID := ws.peer.Sid
	sessionId := ws.GetSessionID()
	raw, err := json.Marshal(map[string]interface{}{kFrom: streamID, kCandidate: web, kTo: PeerId, kSessionId: sessionId})
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kCandidateCommand, Data: raw}
	return ws.conn.WriteJSON(wsm)
}

func (ws *WsWebRTCClientAuthConnection) SendSessionInfo(session string, Sids []media.StreamId) error {
	raw, err := json.Marshal(map[string]interface{}{kFrom: session, kTracks: Sids})
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kSessionInfoCommand, Data: raw}
	return ws.conn.WriteJSON(wsm)
}

func (ws *WsWebRTCClientAuthConnection) SendSessionEnter(session string, Sid media.StreamId) error {
	raw, err := json.Marshal(map[string]interface{}{kFrom: session, kSessionId: Sid})
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kEnterCommand, Data: raw}
	return ws.conn.WriteJSON(wsm)
}

func (ws *WsWebRTCClientAuthConnection) SendSessionLeave(session string, Sid media.StreamId) error {
	raw, err := json.Marshal(map[string]interface{}{kFrom: session, kSessionId: Sid})
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kLeaveCommand, Data: raw}
	return ws.conn.WriteJSON(wsm)
}

func (ws *WsWebRTCClientAuthConnection) SendWebRTCDeInit(web media.WebRTCSubDeInitInfo) error {
	PeerId := PeerId(web.ConnectionId)
	streamID := ws.peer.Sid
	sessionId := ws.GetSessionID()
	raw, err := json.Marshal(map[string]interface{}{kFrom: streamID, kTo: PeerId, kSessionId: sessionId})
	if err != nil {
		return err
	}

	wsm := WSMessage{Type: kByeCommand, Data: raw}
	return ws.conn.WriteJSON(wsm)
}

func (ws *WsWebRTCClientAuthConnection) FindPeer(peer PeerId) bool {
	return ws.peer.Id == peer
}

type WsListConnections map[*WsWebRTCClientAuthConnection]bool
type WSConnections map[media.StreamId]WsListConnections
type WSSessions map[string]WsListConnections

type WSMessage struct {
	Type MessageCommand  `json:"type"`
	Data json.RawMessage `json:"data"`
}

type WSNewMessageData struct {
	Id        PeerId         `json:"from"` // peer
	StreamID  media.StreamId `json:"to"`
	Name      string         `json:"name"`
	UserAgent string         `json:"user_agent"`
}

type WSAnswerMessageData struct {
	Id          PeerId         `json:"from"` // peer
	StreamID    media.StreamId `json:"to"`
	Type        string         `json:"type"`
	Description string         `json:"description"`
}

type WSOfferMessageData struct {
	Id          PeerId         `json:"from"` // peer
	StreamID    media.StreamId `json:"to"`
	Type        string         `json:"type"`
	Description string         `json:"description"`
}

type WSCandidateMessageData struct {
	Id            PeerId         `json:"from"` // peer
	StreamID      media.StreamId `json:"to"`
	SdpMLineIndex int            `json:"sdpMLineIndex"`
	SdpMid        string         `json:"sdpMid"`
	Candidate     string         `json:"candidate"`
}

type WSSessionInfoMessageData struct {
	Id        PeerId         `json:"from"` // peer
	StreamID  media.StreamId `json:"track"`
	SessionID string         `json:"session"`
}

type WSEnterMessageData struct {
	Id        PeerId         `json:"from"` // peer
	StreamID  media.StreamId `json:"track"`
	SessionID string         `json:"session"`
}

type WSLeaveMessageData struct {
	Id        PeerId         `json:"from"` // peer
	StreamID  media.StreamId `json:"track"`
	SessionID string         `json:"session"`
}

type WSByeMessageData struct {
	Id       PeerId         `json:"from"` // peer
	StreamID media.StreamId `json:"to"`
}
