package app

import (
	"encoding/json"
	"errors"
	"gofastocloud_backend/app/errorgt"
	"gofastocloud_backend/app/utils"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_http/fastocloud/public"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type serialPlayerCustom struct {
	ID          *string                `json:"id"`
	Name        string                 `json:"name"`
	Background  string                 `json:"background_url"`
	Icon        string                 `json:"icon"`
	IARC        int                    `json:"iarc"`
	Groups      []string               `json:"groups"`
	Description string                 `json:"description"`
	PrimeDate   gofastogt.UtcTimeMsec  `json:"prime_date"`
	Seasons     []seasonPlayerCustom   `json:"seasons"`
	Price       float64                `json:"price"`
	ViewCount   int                    `json:"view_count"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date"`
	Visible     bool                   `json:"visible"`
}

func (s *serialPlayerCustom) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type seasonPlayerCustom struct {
	ID          *string                `json:"id"`
	Name        string                 `json:"name"`
	Background  string                 `json:"background_url"`
	Icon        string                 `json:"icon"`
	Groups      []string               `json:"groups"`
	Description string                 `json:"description"`
	Season      int                    `json:"season"`
	Episodes    []player.VodInfo       `json:"episodes"`
	ViewCount   int                    `json:"view_count"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date"`
}

func (s *seasonPlayerCustom) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func SerialtoPlayerCustom(serial *front.SerialFront, seasons []seasonPlayerCustom) *serialPlayerCustom {
	return &serialPlayerCustom{
		ID:          serial.ID,
		Name:        serial.Name,
		Background:  serial.Background,
		Icon:        serial.Icon,
		IARC:        serial.IARC,
		Groups:      serial.Groups,
		Description: serial.Description,
		PrimeDate:   serial.PrimeDate,
		Seasons:     seasons,
		Price:       serial.Price,
		ViewCount:   serial.ViewCount,
		CreatedDate: serial.CreatedDate,
		Visible:     serial.Visible,
	}
}

func SeasonToPlayerCustom(season *front.SeasonFront, episodes []player.VodInfo) *seasonPlayerCustom {
	return &seasonPlayerCustom{
		ID:          season.ID,
		Name:        season.Name,
		Background:  season.Background,
		Icon:        season.Icon,
		Groups:      season.Groups,
		Description: season.Description,
		Season:      season.Season,
		Episodes:    episodes,
		ViewCount:   season.ViewCount,
		CreatedDate: season.CreatedDate,
	}
}

func (app *App) DBSeasonAdd(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleAddOrEditSeasonToDB(sBody, w)
}

func (app *App) DBSeasonUpdate(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleAddOrEditSeasonToDB(sBody, w)
}

func (app *App) handleAddOrEditSeasonToDB(body []byte, w http.ResponseWriter) {
	var req front.SeasonFront
	if err := json.Unmarshal(body, &req); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	isEdit := req.ID != nil

	if utils.IsDoublicate(req.Episodes) {
		msg := "season cannot have duplicate episodes"
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	season, err := gofastocloud_models.MakeSeasonFromFront(&req)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if !isEdit {
		season.ID = primitive.NewObjectID()
	}

	dbSerial, err := season.ToByte()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if err := app.storage.SetSeason(season.ID.Hex(), dbSerial); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	front, err := season.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if !isEdit {
		app.wsUpdatesManager.BroadcastSendSeasonDBAdded(front)
		respondWithOk(w)
	} else {
		app.wsUpdatesManager.BroadcastSendSeasonDBUpdated(front)
		respondWithOk(w)
	}
}

func (app *App) DBSeasonRemove(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}
	//find season
	sid := mux.Vars(r)["id"]
	data, err := app.storage.GetSeason(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}
	var season gofastocloud_models.Season
	if err := json.Unmarshal(data, &season); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonParseResponse(&msg))
		return
	}
	//remove season from serial.
	dbSerials, err := app.storage.GetSerials()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}
	for _, dbSerial := range dbSerials {
		var serial gofastocloud_models.Serial
		if err := json.Unmarshal(dbSerial, &serial); err != nil {
			continue
		}
		for i, s := range serial.Seasons {
			if s == season.ID {
				copy(serial.Seasons[i:], serial.Seasons[i+1:])
				serial.Seasons = serial.Seasons[:len(serial.Seasons)-1]
				editSerial, err := serial.ToByte()
				if err != nil {
					msg := err.Error()
					respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonNotFound(&msg))
					return
				}
				front, err := serial.ToFront().ToBytes()
				if err != nil {
					msg := err.Error()
					respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
					return
				}
				if err := app.storage.SetSerial(serial.ID.Hex(), editSerial); err != nil {
					msg := err.Error()
					respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
					return
				}
				app.wsUpdatesManager.BroadcastSendSerialDBUpdated(front)
				break
			}
		}
	}
	//remove season from storege
	front, err := season.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	if err := app.storage.DeleteSeason(sid); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	app.wsUpdatesManager.BroadcastSendSeasonDBRemove(front)
	respondWithOk(w)
}

func (app *App) DBSeasonsList(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	type SeasonsResponce struct {
		Serials []json.RawMessage `json:"seasons"`
	}

	seasons, err := app.getSeasons()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(SeasonsResponce{Serials: seasons}))
}

func (app *App) DBGetSeason(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}
	sid := mux.Vars(r)["id"]
	data, err := app.storage.GetSeason(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}

	var season gofastocloud_models.Season
	if err := json.Unmarshal(data, &season); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonParseResponse(&msg))
		return
	}
	var episodes []player.VodInfo
	for _, e := range season.Episodes {
		episode, err := app.getVodInfoEpisode(e.Hex())
		if err != nil {
			continue
		}

		episodes = append(episodes, *episode)
	}
	seasonPlayer := SeasonToPlayerCustom(season.ToFront(), episodes)
	seasonJSON, err := seasonPlayer.ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(seasonJSON))
}

func (app *App) getSeasons() ([]json.RawMessage, error) {
	dbSeasons, err := app.storage.GetSeasons()
	if err != nil {
		return nil, err
	}

	seasons := []json.RawMessage{}
	for _, s := range dbSeasons {
		var season gofastocloud_models.Season
		if err := json.Unmarshal(s, &season); err != nil {
			continue
		}

		front, err := season.ToFront().ToBytes()
		if err != nil {
			continue
		}
		seasons = append(seasons, front)
	}
	return seasons, nil
}

func (app *App) getVodInfoEpisode(sid string) (*player.VodInfo, error) {
	vodStream, err := app.getEpisode(sid)
	if err != nil {
		return nil, err
	}
	var istream front.IStreamFront
	if err := json.Unmarshal(vodStream, &istream); err != nil {
		return nil, err
	}
	if istream.Type == media.STREAM_TYPE_VOD_PROXY {
		var vod front.VodProxyStreamFront
		if err := json.Unmarshal(vodStream, &vod); err != nil {
			return nil, err
		}
		vodInfo := vod.ToPlayer()
		return &vodInfo, nil
	} else if istream.Type == media.STREAM_TYPE_VOD_RELAY {
		var vod front.VodRelayStreamFront
		if err := json.Unmarshal(vodStream, &vod); err != nil {
			return nil, err
		}
		vodInfo := vod.ToPlayer()
		return &vodInfo, nil
	} else if istream.Type == media.STREAM_TYPE_VOD_ENCODE {
		var vod front.VodEncodeStreamFront
		if err := json.Unmarshal(vodStream, &vod); err != nil {
			return nil, err
		}
		vodInfo := vod.ToPlayer()
		return &vodInfo, nil
	} else {
	}
	return nil, errors.New("invalid stream type")
}

func (app *App) DBSerialAdd(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleAddOrEditSerialToDB(sBody, w)
}

func (app *App) DBSerialUpdate(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	app.handleAddOrEditSerialToDB(sBody, w)
}

func (app *App) handleAddOrEditSerialToDB(body []byte, w http.ResponseWriter) {
	var req front.SerialFront
	if err := json.Unmarshal(body, &req); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}
	isEdit := req.ID != nil

	if utils.IsDoublicate(req.Seasons) {
		msg := "serials cannot have duplicate seasons"
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	serial, err := gofastocloud_models.MakeSerialFromFront(&req)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if !isEdit {
		serial.ID = primitive.NewObjectID()
	}

	dbSerial, err := serial.ToByte()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if err := app.storage.SetSerial(serial.ID.Hex(), dbSerial); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	front, err := serial.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	if !isEdit {
		app.wsUpdatesManager.BroadcastSendSerialDBAdded(front)
		respondWithOk(w)
	} else {
		app.wsUpdatesManager.BroadcastSendSerialDBUpdated(front)
		respondWithOk(w)
	}
}

func (app *App) DBSerialRemove(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sid := mux.Vars(r)["id"]
	data, err := app.storage.GetSerial(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}

	var serial gofastocloud_models.Serial
	if err := json.Unmarshal(data, &serial); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonParseResponse(&msg))
		return
	}

	front, err := serial.ToFront().ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	if err := app.storage.DeleteSerial(sid); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	app.wsUpdatesManager.BroadcastSendSerialDBRemove(front)
	respondWithOk(w)
}

func (app *App) DBSerialsList(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	type SerialsResponce struct {
		Serials []json.RawMessage `json:"serials"`
	}

	serials, err := app.getSerials()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(SerialsResponce{Serials: serials}))
}

func (app *App) DBGetSerial(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	sid := mux.Vars(r)["id"]
	data, err := app.storage.GetSerial(sid)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonNotFound(&msg))
		return
	}

	var serial gofastocloud_models.Serial
	if err := json.Unmarshal(data, &serial); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonParseResponse(&msg))
		return
	}
	//get list seasons in serial
	var seasons []gofastocloud_models.Season
	for _, s := range serial.Seasons {
		data, err := app.storage.GetSeason(s.Hex())
		if err != nil {
			continue
		}
		var season gofastocloud_models.Season
		if err := json.Unmarshal(data, &season); err != nil {
			continue
		}
		seasons = append(seasons, season)
	}
	//get list seasons with episodes
	var seasonJSON []seasonPlayerCustom
	for _, s := range seasons {
		var episodes []player.VodInfo
		for _, e := range s.Episodes {
			episode, err := app.getVodInfoEpisode(e.Hex())
			if err != nil {
				continue
			}
			episodes = append(episodes, *episode)
		}
		seasonPlayer := SeasonToPlayerCustom(s.ToFront(), episodes)
		seasonJSON = append(seasonJSON, *seasonPlayer)
	}

	serialJSON, err := SerialtoPlayerCustom(serial.ToFront(), seasonJSON).ToBytes()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusNotFound, errorgt.MakeErrorJsonParseResponse(&msg))
		return
	}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(serialJSON))
}

func (app *App) getSerials() ([]json.RawMessage, error) {
	dbSerials, err := app.storage.GetSerials()
	if err != nil {
		return nil, err
	}

	serials := []json.RawMessage{}
	for _, s := range dbSerials {
		var serial gofastocloud_models.Serial
		if err := json.Unmarshal(s, &serial); err != nil {
			continue
		}

		front, err := serial.ToFront().ToBytes()
		if err != nil {
			continue
		}
		serials = append(serials, front)
	}
	return serials, nil
}

func (app *App) DBContent(w http.ResponseWriter, r *http.Request) {
	if err := app.auth.ChechIsAuthHttpRequest(r); err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	streams, vods, episodes, err := app.getStreams()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	seasons, err := app.getSeasons()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	serials, err := app.getSerials()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
		return
	}

	content := public.Content{Streams: streams, Vods: vods, Episodes: episodes, Seasons: seasons, Serials: serials}
	respondWithStructJSON(w, http.StatusOK, gofastogt.NewOkResponse(content))
}
