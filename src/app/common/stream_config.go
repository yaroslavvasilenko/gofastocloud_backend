package common

import (
	"encoding/json"

	"gitlab.com/fastogt/gofastocloud/media"
)

type StreamConfig struct {
	config json.RawMessage
	cfg    media.BaseConfig
}

func NewStreamConfig(config json.RawMessage, base media.BaseConfig) *StreamConfig {
	return &StreamConfig{config, base}
}

func (config *StreamConfig) GetConfig() media.BaseConfig {
	return config.cfg
}

func (config *StreamConfig) GetRawConfig() json.RawMessage {
	return config.config
}

type IStreamWatcher interface {
	OnStreamStarted(config *StreamConfig)
}
