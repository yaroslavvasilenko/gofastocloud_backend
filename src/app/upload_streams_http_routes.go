package app

import (
	"encoding/json"
	"fmt"
	"gofastocloud_backend/app/errorgt"
	"io"
	"net/http"
	"time"

	"gitlab.com/fastogt/gofastogt"

	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (app *App) DBPlaylistAdd(w http.ResponseWriter, r *http.Request) {
	err := app.auth.ChechIsAuthHttpRequest(r)
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusUnauthorized, errorgt.MakeErrorJsonAuth(&msg))
		return
	}

	type request struct {
		Type             *media.StreamType `json:"type"`
		StreamLogoUrl    *string           `json:"stream_logo_url"`
		TrailerUrl       *string           `json:"trailer_url"`
		BackgroundUrl    *string           `json:"background_url"`
		ImageTimeToCheck *float32          `json:"image_time_to_check"`
		SkipGroups       *bool             `json:"skip_groups"`
		Groups           []string          `json:"groups"`
		IsSeries         *bool             `json:"is_series"`
	}

	var req request
	var playlist *gofastocloud_base.Playlist
	mr, err := r.MultipartReader()
	if err != nil {
		msg := err.Error()
		respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
		return
	}

	//Read input data
	for {
		part, err := mr.NextPart()
		//If we didn't have parts-break
		if err == io.EOF {
			break
		}

		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
			return
		}

		//Get JSON
		if part.FormName() == "params" {
			if err := json.NewDecoder(part).Decode(&req); err != nil {
				msg := err.Error()
				respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
				return
			}

			if req.Type == nil && req.StreamLogoUrl == nil &&
				req.BackgroundUrl == nil && req.TrailerUrl == nil &&
				req.ImageTimeToCheck == nil && req.SkipGroups == nil && req.Groups == nil {
				msg := "required data"
				respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
				return
			}
		}

		//Get tracks list from files
		if part.FormName() == "file" {
			playlist, err = gofastocloud_base.Parse(part)
			if err != nil {
				msg := "m3u parse"
				respondWithError(w, http.StatusBadRequest, errorgt.MakeErrorJsonInvalidInput(&msg))
				return
			}
		}
	}

	streamLogo := *req.StreamLogoUrl
	skipGroups := *req.SkipGroups
	back := *req.BackgroundUrl
	timeExpire := time.Duration(*req.ImageTimeToCheck)
	trailer := *req.TrailerUrl
	streamType := *req.Type
	isSeries := *req.IsSeries
	//Add stream to store
	if streamType == media.STREAM_TYPE_PROXY {
		app.handleAddProxyStreamsFromPlaylist(playlist, streamLogo, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_VOD_PROXY {
		app.handleAddProxyVodStreamsFromPlaylist(playlist, isSeries, streamLogo, back, trailer, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_RELAY {
		hls := app.config.Settings.HlsHost.GetTrueHost()
		if hls == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", hls.String())
		app.handleAddRelayStreamsFromPlaylist(playlist, origin, streamLogo, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_VOD_RELAY {
		vods := app.config.Settings.VodsHost.GetTrueHost()
		if vods == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", vods.String())
		app.handleAddRelayVodStreamsFromPlaylist(playlist, isSeries, origin, streamLogo, back, trailer, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_COD_RELAY {
		cods := app.config.Settings.CodsHost.GetTrueHost()
		if cods == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", cods.String())
		app.handleAddRelayCodStreamsFromPlaylist(playlist, origin, streamLogo, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_CHANGER_RELAY {
		changer := app.config.Settings.HlsHost.GetTrueHost()
		if changer == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", changer.String())
		app.handleAddRelayChangerStreamsFromPlaylist(playlist, origin, streamLogo, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_ENCODE {
		hls := app.config.Settings.HlsHost.GetTrueHost()
		if hls == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", hls.String())
		app.handleAddEncodeStreamsFromPlaylist(playlist, origin, streamLogo, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_VOD_ENCODE {
		vods := app.config.Settings.VodsHost.GetTrueHost()
		if vods == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", vods.String())
		app.handleAddEncodeVodStreamsFromPlaylist(playlist, isSeries, origin, streamLogo, back, trailer, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_COD_ENCODE {
		cods := app.config.Settings.CodsHost.GetTrueHost()
		if cods == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", cods.String())
		app.handleAddEncodeCodStreamsFromPlaylist(playlist, origin, streamLogo, skipGroups, req.Groups, timeExpire, w)
	} else if streamType == media.STREAM_TYPE_CHANGER_ENCODE {
		changer := app.config.Settings.HlsHost.GetTrueHost()
		if changer == nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		origin := fmt.Sprintf("http://%s", changer.String())
		app.handleAddEncodeChangerStreamsFromPlaylist(playlist, origin, streamLogo, skipGroups, req.Groups, timeExpire, w)
	}
}

func (app *App) handleAddProxyStreamsFromPlaylist(pl *gofastocloud_base.Playlist, streamLogoUrl string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewProxyStream()
		stream.ID = primitive.NewObjectID()

		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.Output = append(stream.Output, *media.NewOutputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddProxyVodStreamsFromPlaylist(pl *gofastocloud_base.Playlist, isSeries bool, streamLogoUrl string, backgroundUrl string, trailerURL string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewVodProxyStream()
		stream.ID = primitive.NewObjectID()

		if isSeries {
			stream.VODType = media.SERIES
		}
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.BackgroundURL = backgroundUrl
		stream.TrailerURL = trailerURL
		stream.Output = append(stream.Output, *media.NewOutputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddRelayStreamsFromPlaylist(pl *gofastocloud_base.Playlist, origin string, streamLogoUrl string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewRelayStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddRelayVodStreamsFromPlaylist(pl *gofastocloud_base.Playlist, isSeries bool, origin string, streamLogoUrl string, backgroundUrl string, trailerURL string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewVodRelayStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()
		if isSeries {
			stream.VODType = media.SERIES
		}

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		stream.BackgroundURL = backgroundUrl
		stream.TrailerURL = trailerURL
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddRelayCodStreamsFromPlaylist(pl *gofastocloud_base.Playlist, origin string, streamLogoUrl string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewCodRelayStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddRelayChangerStreamsFromPlaylist(pl *gofastocloud_base.Playlist, origin string, streamLogoUrl string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewChangerRelayStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddEncodeStreamsFromPlaylist(pl *gofastocloud_base.Playlist, origin string, streamLogoUrl string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewEncodeStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddEncodeVodStreamsFromPlaylist(pl *gofastocloud_base.Playlist, isSeries bool, origin string, streamLogoUrl string, backgroundUrl string, trailerURL string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewVodEncodeStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()
		if isSeries {
			stream.VODType = media.SERIES
		}

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		stream.BackgroundURL = backgroundUrl
		stream.TrailerURL = trailerURL
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddEncodeCodStreamsFromPlaylist(pl *gofastocloud_base.Playlist, origin string, streamLogoUrl string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewCodEncodeStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}

func (app *App) handleAddEncodeChangerStreamsFromPlaylist(pl *gofastocloud_base.Playlist, origin string, streamLogoUrl string, skipGroups bool, groups []string, timeExp time.Duration, w http.ResponseWriter) {
	for oid, track := range pl.Tracks {
		stream := gofastocloud_models.NewChangerEncodeStream()
		stream.Name = track.Name
		stream.TVGLogo = streamLogoUrl
		stream.ID = primitive.NewObjectID()

		// #FIXME http root
		sid := media.StreamId(stream.ID.Hex())
		url := media.MakeHlsOutputUri(origin, stream.TypeStream, sid, oid, "~", media.HLS_PULL, media.HLSSINK, false)

		stream.Output = append(stream.Output, url)
		stream.Input = append(stream.Input, *media.NewInputUri(oid, track.URI))
		for _, tag := range track.Tags {
			if tag.Name == "tvg-id" {
				stream.TVGID = tag.Value
			}
			if tag.Name == "tvg-name" {
				stream.TVGName = tag.Value
			}
			if !skipGroups && tag.Name == "tvg-group" {
				stream.Groups = append(stream.Groups, tag.Value)
			}
			if tag.Name == "tvg-logo" {
				if gofastogt.IsValidUrl(&tag.Value, timeExp) {
					stream.TVGLogo = tag.Value
				}
			}
		}
		stream.Groups = append(stream.Groups, groups...)
		stream.StableForStreaming()

		//Add Stream to store
		store, err := stream.ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		front, err := stream.ToFront().ToBytes()
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}

		// save to store
		err = app.storage.SetStream(stream.ID.Hex(), store)
		if err != nil {
			msg := err.Error()
			respondWithError(w, http.StatusInternalServerError, errorgt.MakeErrorJsonInternal(&msg))
			return
		}
		app.wsUpdatesManager.BroadcastSendStreamDBAdded(front)
	}
	respondWithOk(w)
}
