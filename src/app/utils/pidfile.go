package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"syscall"
)

const pidFileDir string = "/var/run/gofastocloud"
const pidFilePath string = pidFileDir + "/gofastocloud.pid"

func RemovePID() error {
	return os.RemoveAll(pidFilePath)
}

// Write writes a pidfile, returning an error
func WritePID() error {
	filename, err := makePidFilePath()
	if err != nil {
		return fmt.Errorf("create pid file error: %s", err.Error())
	}
	return WriteControl(*filename, os.Getpid())
}

func WriteControl(filename string, pid int) error {
	// Check for existing pid
	oldpid, err := pidfileContents(filename)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	// We have a pid
	if err == nil {
		if pidIsRunning(oldpid) {
			return fmt.Errorf("process is running")
		}
	}

	// We're clear to (over)write the file
	return ioutil.WriteFile(filename, []byte(fmt.Sprintf("%d\n", pid)), 0644)
}

func pidfileContents(filename string) (int, error) {
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return 0, err
	}

	pid, err := strconv.Atoi(strings.TrimSpace(string(contents)))
	if err != nil {
		return 0, fmt.Errorf("pidfile has invalid contents")
	}

	return pid, nil
}

func pidIsRunning(pid int) bool {
	process, err := os.FindProcess(pid)
	if err != nil {
		return false
	}

	err = process.Signal(syscall.Signal(0))

	if err != nil && err.Error() == "no such process" {
		return false
	}

	if err != nil && err.Error() == "os: process already finished" {
		return false
	}

	return true
}

func makePidFilePath() (*string, error) {
	err := os.MkdirAll(pidFileDir, 0777)
	if err != nil {
		return nil, fmt.Errorf("create dir error: %s", err.Error())
	}
	path := pidFilePath
	return &path, nil
}
