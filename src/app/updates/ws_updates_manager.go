package updates

import (
	"encoding/json"
	"sync"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
)

type UpdatesManagerWs struct {
	IWsUpdatesClientConnectionClient

	mutex             sync.Mutex
	updateConnections WsListConnections
}

func NewUpdatesManagerWs() *UpdatesManagerWs {
	manager := UpdatesManagerWs{updateConnections: WsListConnections{}, mutex: sync.Mutex{}}
	return &manager
}

func (app *UpdatesManagerWs) BroadcastMlNotification(notify media.MlNotificationInfo) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendMlNotificationInfo(notify)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastServiceStatistic(statistic ServerStats) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendServiceStatistic(statistic)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastMediaServiceStatistic(statistic media.ServiceStatisticInfo) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendMediaServiceStatistic(statistic)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendStreamDBAdded(stream json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendStreamDBAdded(stream)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendSeasonDBAdded(season json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendSeasonDBAdded(season)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendSeasonDBUpdated(season json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendSeasonDBUpdated(season)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendSeasonDBRemove(season json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendSeasonDBRemoved(season)
	}
	app.mutex.Unlock()
}
func (app *UpdatesManagerWs) BroadcastSendSerialDBAdded(serial json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendSerialDBAdded(serial)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendSerialDBUpdated(serial json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendSerialDBUpdated(serial)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendSerialDBRemove(serial json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendSerialDBRemoved(serial)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendStreamDBUpdated(stream json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendStreamDBUpdated(stream)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastSendStreamDBRemoved(stream json.RawMessage) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendStreamDBRemoved(stream)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastStreamStatistic(statistic media.StreamStatisticInfo) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendStreamStatistic(statistic)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastStreamChangedSources(source media.ChangedSourcesInfo) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendStreamChangedSources(source)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastResultStream(source media.ResultStreamInfo) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendStreamResult(source)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) BroadcastStreamQuitStatus(source media.QuitStatusInfo) {
	app.mutex.Lock()
	for k := range app.updateConnections {
		k.SendStreamQuitStatus(source)
	}
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) CreateWsUpdatesCClientConnection(conn *websocket.Conn) *WsUpdatesClientConnection {
	return NewWsUpdatesClientConnection(conn, app)
}

func (app *UpdatesManagerWs) OnWsUpdatesClientConnectionClose(ws *WsUpdatesClientConnection) {
	app.UnRegisterWsUpdatesClientConnection(ws)
}

func (app *UpdatesManagerWs) RegisterWsUpdatesClientConnection(ws *WsUpdatesClientConnection) {
	if ws == nil {
		return
	}

	app.mutex.Lock()
	app.updateConnections[ws] = true
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) UnRegisterWsUpdatesClientConnection(ws *WsUpdatesClientConnection) {
	if ws == nil {
		return
	}

	app.mutex.Lock()
	delete(app.updateConnections, ws)
	app.mutex.Unlock()
}

func (app *UpdatesManagerWs) MessageUpdatesProcess(rawMessage []byte, w *WsUpdatesClientConnection) error {
	log.Debugf("MessageUpdatesProcess: %s", rawMessage)
	return nil
}
