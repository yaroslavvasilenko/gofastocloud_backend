package updates

import (
	"time"

	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/host"
	"github.com/shirou/gopsutil/v3/mem"
	"github.com/shirou/gopsutil/v3/net"
	"gitlab.com/fastogt/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt"
)

const kExpireTime = 2000000000000

type ServerStats struct {
	Cpu           float64                           `json:"cpu"`
	Gpu           float64                           `json:"gpu"`
	MemoryTotal   uint64                            `json:"memory_total"`
	MemoryFree    uint64                            `json:"memory_free"`
	HddTotal      uint64                            `json:"hdd_total"`
	HddFree       uint64                            `json:"hdd_free"`
	Uptime        gofastogt.UtcTimeMsec             `json:"uptime"`
	Timestamp     gofastogt.UtcTimeMsec             `json:"timestamp"`
	TotalBytesIn  uint64                            `json:"total_bytes_in"`  // number of bytes received
	TotalBytesOut uint64                            `json:"total_bytes_out"` // number of bytes sent
	BandwidthIn   uint64                            `json:"bandwidth_in"`
	BandwidthOut  uint64                            `json:"bandwidth_out"`
	OS            gofastocloud_base.OperationSystem `json:"os"`
	ExpireTime    gofastogt.UtcTimeMsec             `json:"expiration_time"`
	Version       string                            `json:"version"`
	Project       string                            `json:"project"`
}

func MakeDefStats() ServerStats {
	var stats ServerStats
	stats.Project = GetProject()
	stats.Version = GetVersion()
	stats.OS = gofastocloud_base.OperationSystem{}
	stats.ExpireTime = kExpireTime
	stats.Timestamp = gofastogt.Time2UtcTimeMsec(time.Now().Local())
	stats.Cpu = 0
	stats.MemoryTotal, stats.MemoryFree = 0, 0
	stats.HddTotal, stats.HddFree = 0, 0
	stats.Uptime = 0
	stats.TotalBytesOut, stats.TotalBytesIn = 0, 0
	return stats
}

func GetStats(prevStats ServerStats) ServerStats {
	var stats ServerStats
	stats.Project = GetProject()
	stats.Version = GetVersion()
	stats.OS = *GetOS()
	stats.ExpireTime = kExpireTime
	stats.Timestamp = gofastogt.Time2UtcTimeMsec(time.Now().Local())
	stats.Cpu = GetCPUPercent()
	stats.MemoryTotal, stats.MemoryFree = GetMemory()
	stats.HddTotal, stats.HddFree = GetHdd()
	stats.Uptime = GetUptime()
	stats.TotalBytesOut, stats.TotalBytesIn, stats.BandwidthOut, stats.BandwidthIn = GetNetworkStats(prevStats)
	return stats
}

func GetVersionShort() string {
	return kVersionAppShort
}

func GetVersion() string {
	return kVersionApp
}

func GetProject() string {
	return kProjectName
}

func GetCPUPercent() float64 {
	percents, err := cpu.Percent(0, false)
	if err != nil {
		return 0
	}
	return percents[0]
}

func GetMemory() (total, free uint64) {
	memory, err := mem.VirtualMemory()
	if err != nil {
		return 0, 0
	}
	return memory.Total, memory.Available
}

func GetHdd() (total, free uint64) {
	hdd, err := disk.Usage("/")
	if err != nil {
		return 0, 0
	}
	return hdd.Total, hdd.Free
}

func GetOS() *gofastocloud_base.OperationSystem {
	hostInfo, err := host.Info()
	if err != nil {
		return nil
	}
	memory, err := mem.VirtualMemory()
	if err != nil {
		return nil
	}
	var os gofastocloud_base.OperationSystem
	os.Name = humanReadableOSName(hostInfo.OS)
	os.Arch = hostInfo.KernelArch
	os.Version = hostInfo.KernelVersion
	os.RamFree = int64(memory.Free)
	os.RamTotal = int64(memory.Total)
	return &os
}

func humanReadableOSName(name string) string {
	if name == "linux" {
		return "Linux"
	} else if name == "windows" {
		return "Windows NT"
	} else if name == "darwin" {
		return "Mac OS X"
	}
	return "Unknown"
}

func GetUptime() gofastogt.UtcTimeMsec {
	hostInfo, err := host.Info()
	if err != nil {
		return 0
	}
	return gofastogt.UtcTimeMsec(hostInfo.Uptime)
}

func GetNetworkStats(prevStats ServerStats) (bSent, bRecv uint64, speedSent, speedRecv uint64) {
	countersStat, err := net.IOCounters(false)
	if err != nil {
		return 0, 0, 0, 0
	}
	for _, s := range countersStat {
		if s.Name == "all" {
			bSent = s.BytesSent
			bRecv = s.BytesRecv
		}
	}
	bytesSendDiff := bSent - prevStats.TotalBytesOut
	bytesRecvDiff := bRecv - prevStats.TotalBytesIn
	timestampDiff := (gofastogt.Time2UtcTimeMsec(time.Now().Local()) - prevStats.Timestamp) / 1000
	if timestampDiff == 0 {
		return bSent, bRecv, speedSent, speedRecv
	}
	speedSent = bytesSendDiff / uint64(timestampDiff)
	speedRecv = bytesRecvDiff / uint64(timestampDiff)
	return bSent, bRecv, speedSent, speedRecv
}
