package updates

const kProjectName = "GoFastoCloud API"
const kVersionApp = "1.15.0.19 Revision: 497d019a"
const kVersionAppShort = "1.15.0"

// public
const ShareAbsFolder = "/usr/share/gofastocloud"
