package errorgt

import (
	"fmt"

	"gitlab.com/fastogt/gofastogt"
)

var ErrUnknownTypeStream = fmt.Errorf("unknown type of stream")

// examples:
// can't save to database kErrInternal, http.StatusInternalServerError
// try to remove file out of runtime folder kErrForrbiddenAction, http.StatusForbidden
// not implemented may be in future kErrInternalLimitations, http.StatusBadRequest
// failed to decode post body kErrInvalidInput, http.StatusBadRequest

// codes
const (
	kErrInvalidInput      = -5000 //
	kErrInternal          = -5001 //
	kErrParseResponse     = -5002
	kErrForrbiddenAction  = -5003 //
	kErrAuth              = -5004 //
	kErrServiceNotActived = -5005 //
	kErrNotFound          = -5006 //
)

func msgFromDetails(details *string) string {
	if details == nil {
		return "nil"
	}

	return *details
}

func MakeErrorJsonInvalidInput(details *string) gofastogt.ErrorJson {
	msg := fmt.Sprintf("invalid input (%v)", msgFromDetails(details))
	return gofastogt.ErrorJson{Code: kErrInvalidInput, Message: msg}
}

func MakeErrorJsonInternal(details *string) gofastogt.ErrorJson {
	msg := fmt.Sprintf("server internal error (%v)", msgFromDetails(details))
	return gofastogt.ErrorJson{Code: kErrInternal, Message: msg}
}

func MakeErrorJsonParseResponse(details *string) gofastogt.ErrorJson {
	msg := fmt.Sprintf("parse internal service response (%v)", msgFromDetails(details))
	return gofastogt.ErrorJson{Code: kErrParseResponse, Message: msg}
}

func MakeErrorJsonForrbiddenAction(details *string) gofastogt.ErrorJson {
	msg := fmt.Sprintf("forbidden action (%v)", msgFromDetails(details))
	return gofastogt.ErrorJson{Code: kErrForrbiddenAction, Message: msg}
}

func MakeErrorJsonAuth(details *string) gofastogt.ErrorJson {
	msg := fmt.Sprintf("authentificate failed (%v)", msgFromDetails(details))
	return gofastogt.ErrorJson{Code: kErrAuth, Message: msg}
}

func MakeErrorJsonServiceNotActived(details *string) gofastogt.ErrorJson {
	msg := fmt.Sprintf("service not actived (%v)", msgFromDetails(details))
	return gofastogt.ErrorJson{Code: kErrServiceNotActived, Message: msg}
}

func MakeErrorJsonNotFound(details *string) gofastogt.ErrorJson {
	msg := fmt.Sprintf("not found (%v)", msgFromDetails(details))
	return gofastogt.ErrorJson{Code: kErrNotFound, Message: msg}
}
