package webrtc_out

import (
	"encoding/json"
	"sync"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/fastogt/gofastocloud/media"
	"gitlab.com/fastogt/gofastocloud_base"
)

type WebRTCSettings struct {
	Stun string
	Turn string
}

type IWebRTCOut interface {
	WebRTCOutInitStream(web *media.WebRTCOutInitRequest) (*gofastocloud_base.RPCId, error)
	WebRTCOutSDPStream(web *media.WebRTCOutSdpRequest) (*gofastocloud_base.RPCId, error)
	WebRTCOutICEStream(web *media.WebRTCOutIceRequest) (*gofastocloud_base.RPCId, error)
	WebRTCOutDeinitStream(web *media.WebRTCOutDeInitRequest) (*gofastocloud_base.RPCId, error)
}

type WebRTCManagerWs struct {
	IWsWebRTCClientConnectionClient

	mutex             sync.Mutex
	webRTCConnections WSConnections

	client IWebRTCOut

	webrtc *WebRTCSettings
}

func NewWebRTCManagerWs(client IWebRTCOut, webrtc *WebRTCSettings) *WebRTCManagerWs {
	manager := WebRTCManagerWs{webRTCConnections: WSConnections{}, client: client, mutex: sync.Mutex{}, webrtc: webrtc}
	return &manager
}

func (app *WebRTCManagerWs) CreateWsWebRTCClientConnection(conn *websocket.Conn) *WsWebRTCClientAuthConnection {
	return NewWsWebRTCClientAuthConnection(conn, app)
}

func (app *WebRTCManagerWs) GetWsPeerByID(sid media.StreamId, pid PeerId) *WsWebRTCClientAuthConnection {
	app.mutex.Lock()
	defer app.mutex.Unlock()
	peers, ok := app.webRTCConnections[sid]
	if ok {
		for item := range peers {
			if item.FindPeer(pid) {
				return item
			}
		}
	}
	return nil
}

func (app *WebRTCManagerWs) GetSrtreamPeersByID(sid media.StreamId) WsListConnections {
	app.mutex.Lock()
	defer app.mutex.Unlock()
	peers, ok := app.webRTCConnections[sid]
	if ok {
		return peers
	}
	return WsListConnections{}
}

func (app *WebRTCManagerWs) RegisterWsWebRTCClientAuthConnection(ws *WsWebRTCClientAuthConnection, peer *PeerInfo) {
	if ws == nil || peer == nil {
		return
	}

	app.mutex.Lock()
	ws.peer = peer
	if _, ok := app.webRTCConnections[ws.peer.SID]; !ok {
		app.webRTCConnections[ws.peer.SID] = WsListConnections{}
	}
	app.webRTCConnections[ws.peer.SID][ws] = true
	app.mutex.Unlock()

	sid := ws.peer.SID
	pid := ws.peer.ID
	var webrtc *media.WebRTCProp
	if app.webrtc != nil {
		webrtc = &media.WebRTCProp{Stun: app.webrtc.Stun, Turn: app.webrtc.Turn}
	}
	spid := media.WebRTCSubInitRequest{ConnectionId: string(pid), WebRTC: webrtc}
	msg := media.NewWebRTCOutInitRequest(sid, spid)
	app.client.WebRTCOutInitStream(msg)
}

func (app *WebRTCManagerWs) UnRegisterWsWebRTCClientAuthConnection(ws *WsWebRTCClientAuthConnection) {
	if ws == nil || ws.peer == nil {
		return
	}

	sid := ws.peer.SID
	pid := ws.peer.ID
	spid := media.WebRTCSubDeInitRequest{ConnectionId: string(pid)}
	msg := media.NewWebRTCOutDeInitRequest(sid, spid)
	app.client.WebRTCOutDeinitStream(msg)
	app.mutex.Lock()
	delete(app.webRTCConnections[sid], ws)
	ws.peer = nil
	app.mutex.Unlock()
}

func (app *WebRTCManagerWs) OnWsWebRTCClientConnectionClose(ws *WsWebRTCClientAuthConnection) {
	app.UnRegisterWsWebRTCClientAuthConnection(ws)
}

func (app *WebRTCManagerWs) MessageWebRTCProcess(rawMessage []byte, w *WsWebRTCClientAuthConnection) error {
	log.Debugf("MessageWebRTCProcess out: %s", rawMessage)

	var msg WSMessage
	err := json.Unmarshal(rawMessage, &msg)
	if err != nil {
		return err
	}

	if msg.Type == kNewCommand {
		var newMsg WSNewMessageData
		err = json.Unmarshal(msg.Data, &newMsg)
		if err != nil {
			return err
		}

		pid := newMsg.Id
		sid := newMsg.StreamID
		peer := NewPeerInfo(pid, newMsg.UserAgent, sid)
		app.RegisterWsWebRTCClientAuthConnection(w, peer)
	} else if msg.Type == kAnswerCommand {
		var answerMsg WSAnswerMessageData
		err = json.Unmarshal(msg.Data, &answerMsg)
		if err != nil {
			return err
		}

		pid := answerMsg.Id
		sid := answerMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			string_pid := string(pid)
			sdp := media.SessionDescription{ConnectionId: string_pid, SdpDescription: answerMsg.Description, SdpType: answerMsg.Type}
			msg := media.NewWebRTCOutSdpRequest(sid, sdp)
			app.client.WebRTCOutSDPStream(msg)
		}
	} else if msg.Type == kCandidateCommand {
		var candidateMsg WSCandidateMessageData
		err = json.Unmarshal(msg.Data, &candidateMsg)
		if err != nil {
			return err
		}

		pid := candidateMsg.Id
		sid := candidateMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			string_pid := string(pid)
			ice := media.IceCandidate{ConnectionId: string_pid, Candidate: candidateMsg.Candidate, Mlindex: candidateMsg.SdpMLineIndex, Mid: candidateMsg.SdpMid}
			msg := media.NewWebRTCOutIceRequest(sid, ice)
			app.client.WebRTCOutICEStream(msg)
		}
	} else if msg.Type == kByeCommand {
		var byeMsg WSByeMessageData
		err = json.Unmarshal(msg.Data, &byeMsg)
		if err != nil {
			return err
		}

		pid := byeMsg.Id
		sid := byeMsg.StreamID
		peer := app.GetWsPeerByID(sid, pid)
		if peer != nil {
			app.UnRegisterWsWebRTCClientAuthConnection(peer)
		}
	} else {
		log.Warnf("Not handled command type: %s", msg.Type)
	}

	return nil
}
