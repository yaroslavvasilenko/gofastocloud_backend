package app

import (
	"gofastocloud_backend/app/alarm_notification"
	"gofastocloud_backend/app/vod_info"
	"io/ioutil"
	"os"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Settings gofastocloud_models.GoBackendSettings
}

func makeVodInfoByType(info *gofastocloud_models.VodInfoSettings) vod_info.IVodInfo {
	if info != nil {
		if info.Tmdb != nil && info.Type == gofastocloud_models.TMDB {
			return vod_info.TmdbVodInfo{ApiKey: info.Tmdb.Key}
		}
	}
	return vod_info.DummyVodInfo{}
}
func makeAlarmNotificationByType(notification *gofastocloud_models.Notification) alarm_notification.IAlarm {
	if notification == nil {
		return nil
	}

	if notification.Type == gofastocloud_models.EMAIL_NOTIFICATION {
		if notification.EmailService != nil {
			return alarm_notification.EmailAlarm{EmailServerSettings: *notification.EmailService}
		}
	}
	return nil
}

func SaveConfig(s *Config, configPath string) error {
	conf, err := os.Create(configPath)
	if err != nil {
		return err
	}
	defer conf.Close()

	err = yaml.NewEncoder(conf).Encode(s)
	if err != nil {
		return err
	}

	return nil
}

func LoadConfig(configPath string) (*Config, error) {
	buf, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	var s Config
	err = yaml.Unmarshal(buf, &s)
	if err != nil {
		return nil, err
	}
	return &s, nil
}
