package alarm_notification

import "gitlab.com/fastogt/gofastocloud/media"

type IAlarm interface {
	BuildStopStreamMessage(id, name, host, templatePath string, status media.ExitStatus) ([]byte, error)
	SendMessage([]byte, []string) error
}
