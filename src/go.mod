module gofastocloud_backend

go 1.18

require (
	github.com/barasher/go-exiftool v1.8.0
	github.com/cyruzin/golang-tmdb v1.4.4
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/fastogt/pudge v1.0.5
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
	github.com/rs/cors v1.8.2
	github.com/shirou/gopsutil/v3 v3.22.8
	github.com/sirupsen/logrus v1.9.0
	gitlab.com/fastogt/gofastocloud v1.9.26
	gitlab.com/fastogt/gofastocloud_base v1.7.8
	gitlab.com/fastogt/gofastocloud_http v1.4.6
	gitlab.com/fastogt/gofastocloud_models v0.7.10
	gitlab.com/fastogt/gofastogt v1.4.8
	go.mongodb.org/mongo-driver v1.10.2
	gopkg.in/yaml.v3 v3.0.1
	gortc.io/sdp v0.18.2
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/lufia/plan9stats v0.0.0-20220913051719-115f729f3c8c // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/power-devops/perfstat v0.0.0-20220216144756-c35f1ee13d7c // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.5.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/sys v0.0.0-20220919091848-fb04ddd9f9c8 // indirect
)
