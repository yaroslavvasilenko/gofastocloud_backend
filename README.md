# gofastocloud_backend

[![Join the chat at https://discord.com/invite/cnUXsws](https://img.shields.io/discord/584773460585086977?label=discord)](https://discord.com/invite/cnUXsws)

FastoCloud API for media part [COM, PRO, ML] versions

This project help to connect WebRTC streams with FastoCloud, also provide API to work with Streams, Media content, to get service/stream statistics, etc

Install:
```
git clone https://gitlab.com/fastogt/gofastocloud_backend
cd gofastocloud_backend
cp config/gofastocloud.conf /etc/gofastocloud.conf
cd src
go build gofastocloud.go
./gofastocloud
```

Build packages:
```
cmake .. -GNinja -DCMAKE_TOOLCHAIN_FILE=../cmake/go/toolchain/linux.cmake  -DCMAKE_SYSTEM_PROCESSOR=arm -DCPACK_SUPPORT=ON
ninja
cpack -GDEB
```

FastoCloud WIKI:
- https://github.com/fastogt/fastocloud_docs/wiki

Docker:
- https://hub.docker.com/repository/docker/fastogt/gofastocloud

API (Documentation):
- https://fastogt.stoplight.io/docs/fastocloud-api/reference/fastocloud_streams.yaml

Demo:
- https://ws.fastocloud.com

UI:
- https://gitlab.com/fastogt/wsfastocloud
