FROM golang:1.18 AS build

LABEL maintainer="Alexandr Topilski <support@fastogt.com>"

ENV PROJECT_DIR /usr/src/gofastocloud/

WORKDIR $PROJECT_DIR
COPY /src/gofastocloud.go /src/go.mod $PROJECT_DIR
COPY /src/app $PROJECT_DIR/app

# build service
RUN go mod tidy
RUN go build -o /bin/gofastocloud gofastocloud.go
RUN rm -rf $PROJECT_DIR

FROM debian:buster-slim

ENV USER fastocloud

RUN useradd -m -U -d /home/$USER $USER -s /bin/bash

RUN set -ex; \
  BUILD_DEPS='ca-certificates nano exiftool'; \
  PREFIX=/usr/local; \
  apt-get update; \
  apt-get install -y $BUILD_DEPS --no-install-recommends;

# install
COPY src/install /usr/share/gofastocloud/install/
COPY config/gofastocloud.conf /etc/

RUN mkdir /var/run/gofastocloud
RUN chown $USER:$USER /var/run/gofastocloud
RUN chown $USER:$USER /etc/gofastocloud.conf

COPY --from=build /bin/gofastocloud /usr/local/bin/gofastocloud
COPY docker/docker-entrypoint.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

USER $USER
WORKDIR /home/$USER

ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 8088 8010 7010 6010
CMD ["gofastocloud"]
